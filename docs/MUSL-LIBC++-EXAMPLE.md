# Build an example application with C++

This document explains how to build a C++ application using [musl libC](https://git.morello-project.org/morello/musl-libc) for morello and morello-sdk.

## Usage

Create the following workspace structure:

```
cpp/
  |-> workspace/
       |-> sysroot.env
       |-> helloworld/
            |-> Makefile
            |-> main.cpp
  |-> docker-compose.yml
```

Create a `docker-compose.yml` file in `cpp/` as follows:

```
# Docker composer file for Morello LTP
version: '3.8'
services:
  cpp-morello-sdk:
    image: "git.morello-project.org:5050/morello/morello-sdk/morello-sdk:latest"
    container_name: "cpp-morello-sdk"
    volumes:
      - ./workspace:/home/morello/workspace
    tty: true
    restart: unless-stopped
```

Then, bring up the container (from `cpp/)`:
```
$ docker-compose up -d
```

**Note:** It is important to update always to the latest version of the morello-sdk. If you are unsure on which version you are running instead of the command above execute:
```
$ docker-compose pull
$ docker-compose up -d
```

Create a `sysroot.env` file as follows:

```
export MUSL_HOME="/morello/musl"
export MUSL_RPATH="${MUSL_HOME}/lib"
```

Enter `helloworld/` and edit the **Makefile** inserting the code below:
```
# SPDX-License-Identifier: BSD-3-Clause

# This Makefile will compile an helloworld application using Morello Clang and the Musl libc compiled for purecap.

CXX=clang++
# ELF_PATCH is used to check that the correct elf flag for Morello is present
ELF_PATCH=morello_elf
MUSL_HOME?=/morello/musl
MUSL_RPATH?=$(MUSL_HOME)/lib
CLANG_RESOURCE_DIR=$(shell clang -print-resource-dir)

OUT=./bin
# we want the same result no matter where we're cross compiling (x86_64, aarch64)
TARGET?=aarch64-linux-musl_purecap
CPPFLAGS=-lunwind -lc++abi

all:
	mkdir -p $(OUT)
	$(CXX) -c -g -march=morello+c64 \
		--target=$(TARGET) --sysroot $(MUSL_HOME) \
		main.cpp -o $(OUT)/morello-helloworld.cpp.o

	# Use -static to build a static binary and remove "-Wl,--dynamic-linker=$(MUSL_RPATH)/libc.so" and "-Wl,-rpath,$(MUSL_RPATH)"
	$(CXX) -fuse-ld=lld -march=morello+c64 \
		--target=$(TARGET) --sysroot $(MUSL_HOME) \
		$(CPPFLAGS) \
		-rtlib=compiler-rt \
		$(OUT)/morello-helloworld.cpp.o \
		-o $(OUT)/morello-helloworld-c++ \
		-Wl,--dynamic-linker=$(MUSL_RPATH)/libc.so -v \
		-Wl,-rpath,$(MUSL_RPATH)
	$(ELF_PATCH) $(OUT)/morello-helloworld-c++
	rm $(OUT)/morello-helloworld.cpp.o

clean:
	rm $(OUT)/morello-helloworld-c++
```

Edit **main.cpp** and insert the code below:
```
/* SPDX-License-Identifier: BSD-3-Clause */

#include <iostream>

int main(int argc, char **argv)
{ 
    std::cout << "Hello from Morello!!" << std::endl;

	return 0;
}
```

To enter into the container, run the command:

```
$ docker exec -it -u root cpp-morello-sdk /bin/bash
```

At this point you have everything you need. Source the development kit environment file:
```
source /morello/env/morello-sdk
source ../sysroot.env
``` 
and run:
```
make
```
If everything went well your **morello-helloworld-c++** binary for morello should be waiting for you in **workspace/cpp/bin**.

Have a lot of fun!

**Note (1):** `make` can be substituted with `make -j<N>` where **N** is the number of cores.  
**Note (2):** Once you started the docker container the files of your project are accessible at `/home/morello/workspace/cpp`.

For further information please refer to the [Docker](https://docs.docker.com/) documentation.
